# GdkPixbuf Extra Loaders

This project includes GdkPixbuf loaders for less common image formats:

- ANI
- BMP
- ICO
- ICNS
- PNM
- QTIF
- TGA
- XBM
- XPM

## Building GdkPixbuf-Extra

### Requirements

In order to build GdkPixbuf-Extra you will need to have installed:

 - [Meson](http://mesonbuild.com)
 - A C99-compliant compiler and toolchain
 - [GLib's development files](https://gitlab.gnome.org/GNOME/glib/)
 - [GdkPixbuf's development files](https://gitlab.gnome.org/GNOME/gdk-pixbuf/)

### Building and installing

You should use Meson to configure GdkPixbuf-Extra's build, and depending on
the platform you will be able to use Ninja, Visual Studio, or XCode to build
the project; typically, on most platforms, you should be able to use the
following commands to build and install GdkPixbuf in the default prefix:

```sh
$ meson setup _build .
$ meson compile -C _build
$ meson install -C _build
```

You can use Meson's `--prefix` argument to control the installation prefix
at configuration time.

You can also use `meson configure` from within the build directory to
check the current build configuration, and change its options.

#### Build options

For a complete list of build-time options, see the file
[`meson_options.txt`](meson_options.txt).  You can read about Meson
options in general [in the Meson manual](http://mesonbuild.com/Build-options.html).

## Running tests

You can run the test suite by running `meson test -C _build`, where
`_build` is the build directory you used during the build stage.

## License

GdkPixbuf-Extra is released under the terms of the GNU Lesser General Public
License version 2.1, or, at your option, any later version. See the
[COPYING](./COPYING) file for further details.
